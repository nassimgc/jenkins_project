import { expect } from 'chai';
import {my_sum} from '../day-1/exercise-0.js';

describe('day-1',()=>{
    describe('exercise-0 my_sum(a,b)', () =>{
        it('should compute 6+3 = 9',()=>{
            expect(my_sum(6,3)).to.equal(9);
        });
        it('should compute 6+a = 0',()=>{
            expect(my_sum(6,'a')).to.equal(0);
        });

        it('should compute null+null = 0',()=>{
            expect(my_sum()).to.equal(0);
        });
    });
});