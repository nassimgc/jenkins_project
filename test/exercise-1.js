import { expect } from 'chai';
import {my_display_alpha} from '../day-1/exercise-1.js';

describe('day-1',()=>{
    describe('exercise-1 my_display_alpha()', () =>{
        it('should compute alphabet from a to z',()=>{
            expect(my_display_alpha()).to.equal("a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z");
        });
    });
});